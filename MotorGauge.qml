import QtQuick 2.3
import QtQuick.Extras 1.4
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.4

CircularGauge {
    id: gauge

    minimumValue: -10
    maximumValue: 100
    tickmarksVisible: true
    implicitWidth: 1
    implicitHeight: 1

    Layout.fillWidth: true;
    Layout.fillHeight: true;

    value: receiver.data[key] * 100

    property string key

    style: CircularGaugeStyle {
        tickmarkLabel:  Text {
            font.pixelSize: Math.max(6, outerRadius * 0.1)
            text: styleData.value
            color: styleData.value >= 0.8 * gauge.maximumValue  ? "#e34c22" : "#606060"
            antialiasing: true
        }

        tickmark: Rectangle {
            visible: styleData.value % 10 == 0
            implicitWidth: outerRadius * 0.02
            antialiasing: true
            implicitHeight: outerRadius * 0.06
            color: styleData.value >= 0.8 * gauge.maximumValue ? "#e34c22" : "#606060"
        }

        minorTickmark: Rectangle {
            visible: styleData.value < 0.8 * gauge.maximumValue
            implicitWidth: outerRadius * 0.01
            antialiasing: true
            implicitHeight: outerRadius * 0.03
            color: styleData.value >= 0.8 * gauge.maximumValue ? "#e34c22" : "#606060"
        }

        function degreesToRadians(degrees) {
            return degrees * (Math.PI / 180);
        }

        background: Canvas {
            Text {
                text: gauge.key
                font.pixelSize: Math.max(6, outerRadius * 0.1)
                anchors.horizontalCenter: parent.horizontalCenter
                y: parent.height / 3.5
                color: "#c0c0c0"
            }

            onPaint: {
                var ctx = getContext("2d");
                ctx.reset();

                ctx.beginPath();
                ctx.strokeStyle = "#e34c22";
                ctx.lineWidth = outerRadius * 0.02;

                ctx.arc(outerRadius, outerRadius, outerRadius - ctx.lineWidth / 2,
                    degreesToRadians(valueToAngle(80) - 90), degreesToRadians(valueToAngle(100) - 90));
                ctx.stroke();
            }
        }
    }
}
