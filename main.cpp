#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "receiver.h"
#include "sender.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    Receiver recv;
    Sender sender;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("receiver", &recv);
    engine.rootContext()->setContextProperty("sender", &sender);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
