#include "receiver.h"

#include <QDataStream>
#include <QUdpSocket>
#include <QVariantMap>

#include <QtDebug>

Receiver::Receiver(QObject *parent)
    : QObject(parent), socket(new QUdpSocket(this))
{
    if (!socket->bind(QHostAddress::AnyIPv4, 45451, QUdpSocket::ShareAddress)) {
        qFatal("failed bind UDP socket!");
    }
    socket->joinMulticastGroup(QHostAddress("239.255.43.21"));
    qDebug() << "socket status:" << socket->state();
    connect(socket, &QUdpSocket::readyRead, this, &Receiver::processDatagrams);
}

QVariantMap Receiver::data() const
{
    return currentData;
}

void Receiver::processDatagrams()
{
    while (socket->hasPendingDatagrams()) {
        QByteArray datagram;
        datagram.resize(socket->pendingDatagramSize());
        socket->readDatagram(datagram.data(), datagram.size());
        processDatagram(datagram);
    }
}

void Receiver::processDatagram(const QByteArray &data)
{
    QDataStream stream(data);
    stream.setVersion(QDataStream::Qt_5_0);

    QVariantMap map;

    stream >> map;

    currentData = map;
    emit dataChanged(currentData);
}

