import QtQuick 2.3
import QtQuick.Controls 1.4

Item {
    id: pidValueTuner
    property string label
    property int decimals
    property double minimumValue
    property double maximumValue
    property string key

    height: Math.max(labelControl.height, spinbox.height)
    width: 200

    Label {
        id: labelControl

        anchors.left: parent.left
        anchors.verticalCenter: spinbox.verticalCenter

        text: "<b>" + label + ": </b> " + (receiver.data[key] !== undefined ? receiver.data[key].toFixed(4) : 0)
    }
    SpinBox {
        id: spinbox
        anchors.left: parent.left
        anchors.leftMargin: parent.width / 3
        anchors.right: parent.right
        anchors.top: parent.top

        decimals: 4
        minimumValue: 0
        maximumValue: Infinity
        onEditingFinished: {
            sender.send(key + "_Kp", value)
        }

        Component.onCompleted: {
            var initial = receiver.data[key]
            if (initial !== undefined) {
                spinbox.value = initial
            }
        }
    }
}
