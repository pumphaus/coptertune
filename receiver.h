#ifndef RECEIVER_H
#define RECEIVER_H

#include <QObject>
#include <QVariantMap>

class QUdpSocket;

class Receiver : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantMap data READ data NOTIFY dataChanged)
public:
    explicit Receiver(QObject *parent = 0);

    QVariantMap data() const;

signals:
    void dataChanged(const QVariantMap &data);

public slots:
    void processDatagrams();
    void processDatagram(const QByteArray &data);

private:
    QUdpSocket *socket;
    QVariantMap currentData;
};

#endif // RECEIVER_H
