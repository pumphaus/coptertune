#ifndef SENDER_H
#define SENDER_H

#include <QObject>

class QUdpSocket;

class Sender : public QObject
{
    Q_OBJECT
public:
    explicit Sender(QObject *parent = 0);

signals:

public slots:
    void send(const QString &key, const QVariant &value);

private:
    QUdpSocket *socket;
};

#endif // SENDER_H
