import QtQuick 2.3
import QtQuick.Extras 1.4
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

ColumnLayout {
    id: pidTuner
    property string key
    property int decimals
    property double minimumValue
    property double maximumValue

    Layout.fillWidth: true

    ColumnLayout {
        Text {
            text: "<b>" + key + "</b>"
            Layout.alignment: Qt.AlignHCenter
        }
        Rectangle {
            Layout.fillWidth: true
            Layout.leftMargin: 10
            Layout.rightMargin: 10
            height: 1
            color: "#e0e0e0"
        }
        RowLayout {
            Item { Layout.fillWidth: true }
            Label {
                Layout.alignment: Qt.AlignRight
                text: "In Limit:"
            }
            StatusIndicator {
                active: receiver.data[key + "_in_lim"] !== undefined ? receiver.data[key + "_in_lim"] : false
                implicitHeight: 30
                implicitWidth: 30
                Layout.alignment: Qt.AlignLeft
            }
            Item { Layout.fillWidth: true }
            Label {
                Layout.alignment: Qt.AlignRight
                text: "In I-Limit:"
            }
            StatusIndicator {
                active: receiver.data[key + "_in_ilim"] !== undefined ? receiver.data[key + "_in_ilim"] : false
                implicitHeight: 30
                implicitWidth: 30
                Layout.alignment: Qt.AlignLeft
            }
            Item { Layout.fillWidth: true }
        }

        PIDValueTuner {
            label: "Kp"
            key: pidTuner.key + "_Kp"
            Layout.fillWidth: true
        }
        PIDValueTuner {
            label: "Ki"
            key: pidTuner.key + "_Ki"
            Layout.fillWidth: true
        }
        PIDValueTuner {
            label: "Kd"
            key: pidTuner.key + "_Kd"
            Layout.fillWidth: true
        }
        PIDValueTuner {
            label: "Limit"
            key: pidTuner.key + "_lim"
            Layout.fillWidth: true
        }
        PIDValueTuner {
            label: "I-Limit"
            key: pidTuner.key + "_ilim"
            Layout.fillWidth: true
        }
    }
}
