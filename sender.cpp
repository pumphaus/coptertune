#include "sender.h"

#include <QDataStream>
#include <QUdpSocket>

Sender::Sender(QObject *parent)
    : QObject(parent), socket(new QUdpSocket(this))
{
}

void Sender::send(const QString &key, const QVariant &value)
{
    qDebug() << "sending" << key << value;
    QByteArray data;
    QDataStream stream(&data, QIODevice::WriteOnly);
    stream.setVersion(QDataStream::Qt_5_0);
    stream << key << value;
    socket->writeDatagram(data, QHostAddress::Broadcast, 45452);
}
