import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Extras 1.4

ApplicationWindow {
    visible: true
    width: 720 / 2
    height: 1280 / 2
    title: qsTr("CopterTune")

    TabView {
        anchors.fill: parent

        Tab {
            title: "Motors"
            Layout.fillWidth: true
            Layout.fillHeight: true

            GridLayout {
                columns: 2
                anchors.fill: parent

                MotorGauge { key: "throttleFrontLeft" }
                MotorGauge { key: "throttleFrontRight" }
                MotorGauge { key: "throttleBackLeft" }
                MotorGauge { key: "throttleBackRight" }

                Item {
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    height: pitchText.height * 1.1
                    Label {
                        id: pitchText
                        anchors.top: parent.top
                        anchors.right: parent.horizontalCenter
                        anchors.rightMargin: 10
                        text: "Pitch: " + (receiver.data["pitch"] !== undefined ? receiver.data["pitch"].toFixed(4) : 0)
                    }
                    Label {
                        anchors.top: parent.top
                        anchors.left: parent.horizontalCenter
                        anchors.leftMargin: 10
                        text: "Roll: " + (receiver.data["roll"] !== undefined ? receiver.data["roll"].toFixed(4) : 0)
                    }
                }

                RowLayout {
                    Layout.columnSpan: 2
                    Item { Layout.fillWidth: true }
                    Button {
                        text: "Quit"
                        onClicked: Qt.quit()
                    }
                    Item { Layout.fillWidth: true }
                }
            }
        }

        Tab {
            title : "Rate PIDs"
            id: rateTab
            ScrollView {
                id: scrollView
                ColumnLayout {
                    width: scrollView.viewport.width
                    PIDTuner {
                        minimumValue: 0
                        maximumValue: 0.01
                        decimals: 4
                        key: "pitchRatePID"
                    }
                    PIDTuner {
                        minimumValue: 0
                        maximumValue: 0.01
                        decimals: 4
                        key: "rollRatePID"
                    }
                    PIDTuner {
                        minimumValue: 0
                        maximumValue: 0.01
                        decimals: 4
                        key: "yawRatePID"
                    }
                }
            }
        }

        Tab {
            title : "Attitude PIDs"
            id: attitudeTab
            ScrollView {
                anchors.fill: parent
                ColumnLayout {
                    width: attitudeTab.width
                    PIDTuner {
                        minimumValue: 0
                        maximumValue: 3
                        decimals: 2
                        key: "pitchAttitudePID"
                    }
                    PIDTuner {
                        minimumValue: 0
                        maximumValue: 3
                        decimals: 2
                        key: "rollAttitudePID"
                    }
                }
            }
        }
    }
}
